/*
Rule MQTT
Untuk menghidupkan lampu, kirim sign 1
Untuk mematikan statu lampu, kirim sign 0
Untuk PB 1, jika di tekan di widget, akan mengirimkan angka 1
Untuk PB 2, jika di tekan di widget, akan mengirimkan angka 2

Untuk menampilkan suhu, ada pada baris 102

WARNING !!!!
Jangan lupa ganti nama SSID dan password sesuai dengan yang anda miliki

TODO
* Untuk update data, ada pada fungsi, MQTTpub()
* Untuk menerima data dari widget, ada pada fungsi MQTTcallback()

Selamat bekerja

*/

#include <Arduino.h>

// WiFi
#include <WiFi.h>
WiFiClient espClient;
const char *SSID = "TERSERAH";
const char *PASSWORD = "qwertyuiop2";

// MQTT Protocol
#include <PubSubClient.h>
PubSubClient MQTT_client(espClient);
#define MQTT_HOST "103.176.79.5"
#define MQTT_PORT 1887

// Library Sensor
#include <OneWire.h>
#include <DallasTemperature.h>
#define ONE_WIRE_BUS GPIO_NUM_4
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature DS18B20(&oneWire);

// Relay
#define RELAY_CH01 GPIO_NUM_16
#define RELAY_CH02 GPIO_NUM_17
#define RELAY_CH03 GPIO_NUM_18
#define RELAY_CH04 GPIO_NUM_19

// Push button
#define PUSH_BUTTON1 GPIO_NUM_21
#define PUSH_BUTTON2 GPIO_NUM_22

// Proto fucntion
uint8_t readTemperatureSensor();
void wifiInit();
void MQTTinit();
void MQTTcallback(char *topic, byte *payload, unsigned int length);
void MQTTsub(const char *topic);
void MQTTreconnect();
void MQTTloop();
void MQTTpub();

uint32_t previous_millis;

void setup()
{
  // Setup Serialport
  Serial.begin(115200);

  // Setup sensor
  Serial.println("[DBG] Setup Sensor");
  DS18B20.begin();

  // Setup OUTPUT
  Serial.println("[DBG] Setup Ouput");
  pinMode(RELAY_CH01, OUTPUT);
  pinMode(RELAY_CH02, OUTPUT);
  pinMode(RELAY_CH03, OUTPUT);
  pinMode(RELAY_CH04, OUTPUT);

  // Setup INPUT
  Serial.println("[DBG] Setup Input");
  pinMode(PUSH_BUTTON1, INPUT);
  pinMode(PUSH_BUTTON2, INPUT);

  // Setup WiFi
  Serial.println("[DBG] Setup WiFi");
  wifiInit();

  // Setup MQTT
  Serial.println("[DBG] Setup MQTT");
  MQTTinit();

  Serial.println("[DBG] Setup Done..!");
}

/*
Silahkan tulis program didalam fungsi loop.
Jangan menambahkan fungsi yang baru.ß
 */
void loop()
{
  // Code here..

  // Menampilkan suhu
  Serial.println("[DBG] TempSensor: " + (String)readTemperatureSensor());

  // MQTT Handler
  MQTTloop();
  MQTTreconnect();

  // Mengirimkan data selama 3 detik sekali
  if (millis() - previous_millis >= 3000)
  {
    // Update data
    MQTTpub();
    // Serial.println("[DBG] MQTT Subscribe");
    MQTTsub("1_pb");
    previous_millis = millis();
  }
}

// Init MQTT client
void MQTTinit()
{
  // Init device
  MQTT_client.setServer(MQTT_HOST, MQTT_PORT);
  MQTT_client.setCallback(MQTTcallback);
}

// Init MQTT system
void MQTTreconnect()
{
  if (!MQTT_client.connected())
  {
    Serial.println("Reconnect process");
    if (MQTT_client.connect("LKS-SMKN5"))
    {
      Serial.println("MQTT connected");
    }
    else
    {
      Serial.println("MQTT Fail connected");
      Serial.print(MQTT_client.state());
      delay(1000);
    }
  }
}

// MQTT Callback (Silahkan kembangkan fungsi ini untuk menerima data)
void MQTTcallback(char *topic, byte *payload, unsigned int length)
{
  // Call back process
  Serial.print("Message arrived in topic: ");
  Serial.println(topic);
  Serial.print("Message:");
  for (int i = 0; i < length; i++)
  {
    Serial.println((char)payload[i]);
  }
}

// Function subscribe
void MQTTsub(const char *topic)
{
  MQTT_client.subscribe(topic);
}

// Function publish (Silahkan kembangkan fungsi ini untuk mengirim data)
void MQTTpub()
{
  if (MQTT_client.connected())
  {
    MQTT_client.publish("1_sensor", "12"); // Kirim param sensor dengan angka 12
    MQTT_client.publish("1_lampu1", "0");  // 1 untuk on, 0 untuk off
    MQTT_client.publish("1_lampu2", "0");
  }
}

// Loop MQTT server
void MQTTloop()
{
  MQTT_client.loop();
}

// WiFi Setup
void wifiInit()
{
  // set wifi
  WiFi.begin(SSID, PASSWORD);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
  }
}

// Fungsi membaca sensor suhu
uint8_t readTemperatureSensor()
{
  DS18B20.requestTemperatures();
  return DS18B20.getTempCByIndex(0);
}